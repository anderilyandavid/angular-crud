var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json())

const cors = require('cors')
const corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200
}

app.use(cors(corsOptions))


require('./app/route/customer.route.js')(app);

// Create a Server
app.listen(3000,()=>console.log('Expres server is running at port no : 3000'));

